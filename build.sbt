organization := "org.brace"

name := "nittei"

val base_version = "0.1"

scalaVersion := "2.11.6"

crossPaths := false

scalacOptions := Seq("-feature", "-unchecked", "-deprecation", "-encoding", "utf8")

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

fork in run := true

libraryDependencies ++= {
  Seq(
  "org.scalatest" %% "scalatest" % "2.2.6" % "test"
  )
}

testOptions in Test += Tests.Argument("-oI")

coverageEnabled in Test := true

coverageMinimum := 70

coverageFailOnMinimum := true

coverageHighlighting := {
  if(scalaBinaryVersion.value == "2.11") true
  else false
}

logBuffered in Test := false

jarName in assembly := s"${name.value}_${scalaVersion.value}-assembly-${version.value}.jar"

assemblyMergeStrategy in assembly := {
  case "log4j.properties" => MergeStrategy.discard
  case "application.conf" => MergeStrategy.discard
  case "dev.properties" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}