package org.brace.nittei.scheduler

case class ScheduleItem(due: java.util.Date, effort: Long, weight: Int, task: String)