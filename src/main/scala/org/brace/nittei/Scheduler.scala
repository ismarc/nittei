package org.brace.nittei

import org.brace.nittei.scheduler.ScheduleItem

class Scheduler {
  def schedule(items: List[ScheduleItem]): List[ScheduleItem] = {
    List[ScheduleItem]()
  }
  
  /** Sort a list of [[ScheduleItem]]s based on the modified due date scheduling heuristic.
   * @param items The list of items to schedule
   * @param mddFunc The function to use to calculate the modified due date
   * @param processed The optional unit start time.
   */
  def mddSort(items: List[ScheduleItem], mddFunc: (java.util.Date, ScheduleItem) => Float, processed: java.util.Date = new java.util.Date(0)): List[ScheduleItem]  = {
    val bestTask :: rest = items.sortWith((x, y) => x.due.getTime() < y.due.getTime())
    val bestMdd = mddFunc(processed, bestTask)
    val best = rest.foldLeft((bestMdd, bestTask))((result, entry) => {
      val mddEntry = mddFunc(processed, entry)
      if (mddEntry < bestMdd) {
        (mddEntry, entry)
      } else {
        result
      }
    })
    val remaining = items.filter(_ != best._2)

    if (items.size == 1) {
      best._2 :: remaining
    } else {
      best._2 :: mddSort(remaining, mddFunc, new java.util.Date(processed.getTime() + best._2.effort))
    }
  }
  
  /** Calculate a modified due date based on the current unit time and a given item.
   * @param time The current unit time
   * @param item The [[ScheduleItem]] to calculate against
   */
  def mdd(time: java.util.Date, item: ScheduleItem): Float = {
    (time.getTime() + item.effort).max(item.due.getTime()).toFloat
  }

  /** Calculate a weighted modified due date based on the current unit time and a given item.
   * @param time the current unit time
   * @param item the [[ScheduleItem]] to calculate against
   */
  def wmdd(time: java.util.Date, item: ScheduleItem): Float = {
    val weightedTime = (1 / item.weight.toFloat) * item.effort.max(item.due.getTime() - time.getTime()).toFloat
    weightedTime
  }
}