package org.brace.nittei.test

import org.brace.nittei.Scheduler
import org.brace.nittei.scheduler.ScheduleItem

import org.scalatest.FeatureSpec

class SchedulerTest extends FeatureSpec {
  feature("Modified due date") {
    scenario("returns original due date properly") {
      val scheduler = new Scheduler()
      val mdd = scheduler.mdd(new java.util.Date(0), ScheduleItem(new java.util.Date(100), 1, 0, ""))
      assert(mdd == 100)
    }
    scenario("returns modified date when date after due") {
      val scheduler = new Scheduler()
      val mdd = scheduler.mdd(new java.util.Date(100), ScheduleItem(new java.util.Date(0), 1, 0, ""))
      assert(mdd == 101)
    }
    scenario("returns modified date when calculated date after due date") {
      val scheduler = new Scheduler()
      val mdd = scheduler.mdd(new java.util.Date(25), ScheduleItem(new java.util.Date(20), 10, 0, ""))
      assert(mdd == 35)
    }
  }
  feature("Weighted modified due date") {
    scenario("returns original due date properly") {
      val scheduler = new Scheduler()
      val wmdd = scheduler.wmdd(new java.util.Date(0), ScheduleItem(new java.util.Date(100), 1, 1, ""))
      assert(wmdd == 100)
    }
    scenario("returns modified date when date after due") {
      val scheduler = new Scheduler()
      val wmdd = scheduler.wmdd(new java.util.Date(100), ScheduleItem(new java.util.Date(0), 1, 1, ""))
      assert(wmdd == 1.0)
    }
    scenario("returns modified date when calculated after due date") {
      val scheduler = new Scheduler()
      val wmdd = scheduler.wmdd(new java.util.Date(25), ScheduleItem(new java.util.Date(20), 10, 1, ""))
      assert(wmdd == 10.0)
    }
    scenario("properly weights modified date") {
      val scheduler = new Scheduler()
      val wmdd = scheduler.wmdd(new java.util.Date(0), ScheduleItem(new java.util.Date(100), 1, 2, ""))
      assert(wmdd == 50.0)
    }
  }
}